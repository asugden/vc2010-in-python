from class_CellM import CellM
from neuron import h as nrn

class RS(InhMulti):
    def __init__(self, pos):
        self.parameters()
        
        # InhMulti.__init__(self, pos, L, diam, Ra, cm)
        InhMulti.__init__(self, pos, 'RS')

        self.create_dends(self.dendrite_section_properties, self.biophysics_all['cm']) # this is in InhMulti()
        self.connect_sections()
        self.biophys()
        self.synapse_create()
        
        # what's left? create list of presynaptic cells
    
    # PARAMETERS ------------------------------------------------------------------------
    def parameters(self):
        # name, length, diameter
        self.dendrite_section_properties = [
            ('apical_trunk', 35., 2.5),
            ('apical_1', 180., 2.4),
            ('apical_tuft', 140., 2.),
            ('apical_oblique', 200., 2.3),
            
            ('basal_1', 50., 2.5),
            ('basal_2', 150., 1.6),
            ('basal_3', 150, 1.6),
        ]
        
        # connections by name, doesn't slow things down much
        # (connect me, at position, to her, at position)
        # subsets are now 0-4, 5-9, 10-14 describing each of the dendrites branching from the soma
        self.dendrite_connections = [
            ('apical_trunk', 0, 'soma', 1),
            ('apical_1', 0, 'apical_trunk', 1),
            ('apical_tuft', 0, 'apical_1', 1),
            ('apical_oblique', 0, 'apical_trunk', 1),
            
            ('basal_1', 0, 'soma', 0),
            ('basal_2', 0, 'basal_1', 1),
            ('basal_3', 0, 'basal_1', 1),
        ]
        
        
        # list of receptors to add to all dendritic sections
        self.receptors = ['ampa', 'ampaf', 'ampad', 'nmda', 'gabaa', 'gabab', 'gabaaf', 'gababf', 'gabaad', 'gababd']

        # list channels to insert everywhere
        self.channels = ['pas', 'kv', 'na', 'ca', 'kca', 'km', 'cad']
        
        # biophysics adjustments to be applied to all compartments, soma, and dendrites
        self.biophysics_all = {
            'g_pas':4e-5,
            'e_pas':-65,
            'Ra':200,
            'cm':2.065,
            'ek':-75,
            'ena':60,
            'gbar_kv':800,
            'gbar_na':5000,
            'gbar_km':10,
        }
        
        self.biophysics_soma = {
            'L':13,
            'diam':15.6,
            'gbar_ca':60,
            'gbar_kca':40,
            'taur_cad':100,
        }
        
        self.biophysics_dendrites = {
            'gbar_ca':30,
            'gbar_kca':20,
        }

    # END PARAMETERS --------------------------------------------------------------------
    
    # connect sections of this cell together
    def connect_sections(self):
        # set up a dictionary converting dendritic section names to numbers-- fast
        matching_dict = dict()
        for i in range(len(self.dendrite_section_properties)):
            matching_dict[self.dendrite_section_properties[i][0]] = i
        
        # connect sections together appropriately
        # connect(parent, parent_end, {child_start=0})
        for connect in self.dendrite_connections:
            dend_ind = matching_dict[connect[0]]
            if connect[2] == 'soma':
                self.list_dend[matching_dict[connect[0]]].connect(self.soma, connect[3], connect[1])
            else:
                parent_ind = matching_dict[connect[2]]
                self.list_dend[dend_ind].connect(self.list_dend[parent_ind], connect[3], connect[1])
    
    # add all of the necessary receptors
    def biophys(self):
        # soma
        for channel in self.channels:
            self.soma.insert(channel)
        for key in self.biophysics_all:
            setattr(self.soma, key, self.biophysics_all[key])
        for key in self.biophysics_soma:
            setattr(self.soma, key, self.biophysics_soma[key])
        # nrn.ion_style("ca_ion",0,1,0,0,0)

        # dendrites
        # set the distance off of the soma
        nrn.distance(sec = self.soma)
        
        for sect in self.list_dend:
            # this may cause problems
            # look here if differences between hoch and python versions
            # check h-current level in original hoch
            for channel in self.channels:
                sect.insert(channel)
            for key in self.biophysics_all:
                setattr(sect, key, self.biophysics_all[key])
            for key in self.biophysics_dendrites:
                setattr(sect, key, self.biophysics_dendrites[key])
            # nrn.ion_style("ca_ion",0,1,0,0,0)
    
    # create all possible synapses, exactly as done in the neuron code 
    def synapse_create(self):
        # synapses is a dict so that it can include all types of synapses referenced by name
        self.synapses = dict()
        # iterate through all types of receptors and add them to each dendritic compartment
        for receptor in self.receptors:
            self.synapses[receptor] = []
            for dendrite in self.list_dend:
                # ??? this may need to be 'Cell' not 'self' ???
                self.synapses[receptor].append(getattr(self, 'syn_'+receptor+'_create')(dendrite(0.5)))
    
    # set up connections eventually            
    def connect(self):
        pass

if __name__ == '__main__':
    nrn.load_file('stdrun.hoc')
    pos = (0., 0., 0.)
    cell = RS(pos)
    # temp is important
    nrn.celsius = 30
    
    stim = nrn.IClamp(cell.soma(0.5))
    stim.delay = 50
    stim.dur = 200
    stim.amp = 0.5
    nrn.tstop = 400
