# class_cellm.py - a cell-wide class, of which FS, LTS, and RS are sub-classes
# branched from class_cell due to removing attributes specific to L2_Pyr and L5_Pyr in 9/2012
# WARNING: synapse classes copied from VC2010 but are not necessarily complete, see below
#
# v 1.0
# rev 2013-01-29 (AS: created)

from neuron import h as nrn

# Units for e: mV
# Units for gbar: S/cm^2 unless otherwise noted
# Class for both LTS and multicompartment FS 
# cm is capacitance are uF/cm^2 and value is almost always 0.7-1 (or 2 in the case of 2009 on)

# =======================================================================================
# INHMULTI description

class CellM():
    def __init__(self, pos, cell_name='Cell'):
        # set position and name
        self.pos = pos
        self.name = cell_name

        # create soma
        self.soma = nrn.Section(cell=self, name=cell_name+'_soma')

    # Creates dendritic sections
    # dend_props is a list of tuples containing name, length, diameter of each dendrite
    # the length of dend_props defines the number of dendritic compartments
    def create_dends(self, dend_props, cm):
        # preallocate list to store dends
        self.list_dend = []
 
        # create dends and set dend props
        for sec_name, L, diam in dend_props:
            # the following line is the one that makes the Neuron section
            self.list_dend.append(nrn.Section(name=self.name+sec_name))
            
            # the -1 index always changes the last element of the list,
            # i.e. if the list is [1,2,3], then the -1 element of list is 3
            self.list_dend[-1].L = L
            self.list_dend[-1].diam = diam
            self.list_dend[-1].Ra = 200
            self.list_dend[-1].cm = cm

            # set nseg for each dend
            # this code sets multiple compartments when the length is greater than 50 um
            if L > 100:
                self.list_dend[-1].nseg = int(L / 50)
                
                # make dend.nseg odd for all sections
                #if not self.list_dend[-1].nseg % 2:
                #    self.list_dend[-1].nseg += 1
    
    # [', 'gababf', 'gabaad', 'gababd']
    
    ## For all synapses, section location 'secloc' is being explicitly supplied 
    ## for clarity, even though they are (right now) always 0.5. Might change in future
    # creates a RECEIVING inhibitory synapse at secloc
    def syn_gabaa_create(self, secloc):
        syn_gabaa = nrn.Exp2Syn(secloc)
        syn_gabaa.e = -80
        syn_gabaa.tau1 = 0.5
        syn_gabaa.tau2 = 5
        return syn_gabaa

    # creates a RECEIVING slow inhibitory synapse at secloc
    # called: self.soma_gabab = syn_gabab_create(self.soma(0.5))
    def syn_gabab_create(self, secloc):
        syn_gabab = nrn.Exp2Syn(secloc)
        syn_gabab.e = -80
        syn_gabab.tau1 = 1
        syn_gabab.tau2 = 20
        return syn_gabab

    # creates a RECEIVING excitatory synapse at secloc
    def syn_ampa_create(self, secloc):
        syn_ampa = nrn.Exp2Syn(secloc)
        syn_ampa.e = 0
        syn_ampa.tau1 = 0.5
        syn_ampa.tau2 = 5
        return syn_ampa
        
    # creates a RECEIVING facilitating excitatory synapse at secloc
    def syn_ampaf_create(self, secloc):
        syn_ampaf = nrn.FDSExp2Syn(secloc)
        syn_ampaf.e = 0
        syn_ampaf.tau1 = 0.5
        syn_ampaf.tau2 = 3
        syn_ampaf.f = .2
        syn_ampaf.tau_F = 200
        syn_ampaf.d1 = 1
        syn_ampaf.tau_D1 = 380
        syn_ampaf.d2 = 1
        syn_ampaf.tau_D2 = 9200
        return syn_ampaf

    # creates a RECEIVING depressing excitatory synapse at secloc
    def syn_ampad_create(self, secloc):
        syn_ampad = nrn.FDSExp2Syn(secloc)
        syn_ampad.e = 0
        syn_ampad.tau1 = 0.5
        syn_ampad.tau2 = 3
        syn_ampad.f=.5
        syn_ampad.tau_F = 94
        syn_ampad.d1 = 0.416
        syn_ampad.tau_D1 = 380
        syn_ampad.d2 = 0.975
        syn_ampad.tau_D2 = 9200
        return syn_ampad

    # creates a RECEIVING nmda synapse at secloc
    # this is a pretty fast NMDA, no?
    def syn_nmda_create(self, secloc):
        syn_nmda = nrn.Exp2Syn(secloc)
        syn_nmda.tau1 = 1
        syn_nmda.tau2 = 20
        return syn_nmda
        
    # creates a receiving facilitating gaba_a synapse at position secloc
    def syn_gabaaf_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 0.5
        s.tau2 = 10
        s.e = -80
        s.f = .07
        s.tau_F = 94
        s.d1 = .9
        s.tau_D1 = 380
        s.d2 = 1
        s.tau_D2 = 9200
    
    # creates a receiving depressing gaba_a synapse at position secloc
    def syn_gabaad_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 0.5
        s.tau2 = 10
        s.e = -80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
        
    # creates a receiving facilitating gaba_b synapse at position secloc
    def syn_gababf_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1=1
        s.tau2=20
        s.e=-80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
    
    # creates a receiving depressing gaba_b synapse at position secloc WARNING, this is the same as gababf
    def syn_gababd_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 1
        s.tau2 = 20
        s.e = -80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
    
    

    # connect_to_target created for pc, used in Network()
    # these are SOURCES of spikes
    def connect_to_target(self, target):
        nc = nrn.NetCon(self.soma(0.5)._ref_v, target, sec=self.soma)
        nc.threshold = 0

        return nc

