# Exact duplicate of VC2010 FS cell (although in InhMulti, the compartments will be changed to be always odd)

from class_CellM import CellM
from neuron import h as nrn

class FS(InhMulti):
    def __init__(self, pos):
        self.parameters()
        
        # InhMulti.__init__(self, pos, L, diam, Ra, cm)
        InhMulti.__init__(self, pos, 'FS')

        self.create_dends(self.dendrite_section_properties, self.biophysics_all['cm']) # this is in InhMulti()
        self.connect_sections()
        self.biophys()
        self.synapse_create()
        
        # what's left? create list of presynaptic cells
    
    # PARAMETERS ------------------------------------------------------------------------
    def parameters(self):
        # name, length, diameter
        self.dendrite_section_properties = [
            ('a', 3.4, 3),
            ('b', 8.4, 2),
            ('c', 143, 1.25),
            ('d', 117, 1.25),
            ('e', 140, 1.25),
            
            ('f', 3.4, 3),
            ('g', 8.4, 2),
            ('h', 143, 1.25),
            ('i', 117, 1.25),
            ('j', 140, 1.25),
            
            ('k', 3.4, 3),
            ('l', 8.4, 2),
            ('m', 143, 1.25),
            ('n', 117, 1.25),
            ('o', 140, 1.25),
        ]
        
        # connections by name, doesn't slow things down much
        # (connect me, at position, to her, at position)
        # subsets are now 0-4, 5-9, 10-14 describing each of the dendrites branching from the soma
        self.dendrite_connections = [
            ('a', 0, 'soma', 1),
            ('b', 0, 'a', 1),
            ('c', 0, 'b', 1),
            ('d', 0, 'b', 1),
            ('e', 0, 'a', 1),
            
            ('f', 0, 'soma', 1),
            ('g', 0, 'f', 1),
            ('h', 0, 'g', 1),
            ('i', 0, 'g', 1),
            ('j', 0, 'f', 1),
            
            ('k', 0, 'soma', 1),
            ('l', 0, 'k', 1),
            ('m', 0, 'l', 1),
            ('n', 0, 'l', 1),
            ('o', 0, 'k', 1),
        ]
        
        
        # list of receptors to add to all dendritic sections
        self.receptors = ['ampa', 'nmda', 'gabaa', 'gabab', 'ampaf', 'ampad']

        # list channels to insert everywhere
        self.channels = ['pas', 'kv', 'na', 'ca', 'kca', 'km']
        
        # biophysics adjustments to be applied to all compartments, soma, and dendrites
        self.biophysics_all = {
            'g_pas':1.2e-4,
            'e_pas':-73,
            'Ra':200,
            'cm':1,
            'ek':-55,
            'ena':75,
            #'vshift':0,
            'eca':140,
        }
        
        self.biophysics_soma = {
            'L':8.2,
            'diam':13.2,
            'gbar_na':600,
            'gbar_kv':500,
            'gbar_ca':0,
            'gbar_kca':0,
            'gbar_km':0,
        }
        
        self.biophysics_dendrites = {
            'gbar_na':350,
            'gbar_kv':350,
            'gbar_ca':0,
            'gbar_kca':0,
            'gbar_km':0,
        }
        # ??? What is ion style ??? ion_style("ca_ion",0,1,0,0,0) // from original Mainen patdemo code
    # END PARAMETERS --------------------------------------------------------------------
    
    # connect sections of this cell together
    def connect_sections(self):
        # set up a dictionary converting dendritic section names to numbers-- fast
        matching_dict = dict()
        for i in range(len(self.dendrite_section_properties)):
            matching_dict[self.dendrite_section_properties[i][0]] = i
        
        # connect sections together appropriately
        # connect(parent, parent_end, {child_start=0})
        for connect in self.dendrite_connections:
            dend_ind = matching_dict[connect[0]]
            if connect[2] == 'soma':
                self.list_dend[matching_dict[connect[0]]].connect(self.soma, connect[3], connect[1])
            else:
                parent_ind = matching_dict[connect[2]]
                self.list_dend[dend_ind].connect(self.list_dend[parent_ind], connect[3], connect[1])
    
    # add all of the necessary receptors
    def biophys(self):
        # soma
        for channel in self.channels:
            self.soma.insert(channel)
        for key in self.biophysics_all:
            setattr(self.soma, key, self.biophysics_all[key])
        for key in self.biophysics_soma:
            setattr(self.soma, key, self.biophysics_soma[key])
        # nrn.ion_style("ca_ion",0,1,0,0,0)

        # dendrites
        # set the distance off of the soma
        nrn.distance(sec = self.soma)
        
        for sect in self.list_dend:
            # this may cause problems
            # look here if differences between hoch and python versions
            # check h-current level in original hoch
            for channel in self.channels:
                sect.insert(channel)
            for key in self.biophysics_all:
                setattr(sect, key, self.biophysics_all[key])
            for key in self.biophysics_dendrites:
                setattr(sect, key, self.biophysics_dendrites[key])
            # nrn.ion_style("ca_ion",0,1,0,0,0)
    
    # create all possible synapses, exactly as done in the neuron code 
    def synapse_create(self):
        # synapses is a dict so that it can include all types of synapses referenced by name
        self.synapses = dict()
        # iterate through all types of receptors and add them to each dendritic compartment
        for receptor in self.receptors:
            self.synapses[receptor] = []
            for dendrite in self.list_dend:
                # ??? this may need to be 'Cell' not 'self' ???
                self.synapses[receptor].append(getattr(self, 'syn_'+receptor+'_create')(dendrite(0.5)))
    
    # set up connections eventually            
    def connect(self):
        pass

if __name__ == '__main__':
    nrn.load_file('stdrun.hoc')
    pos = (0., 0., 0.)
    cell = FS(pos)
    # temp is important
    nrn.celsius = 30
    
    stim = nrn.IClamp(cell.soma(0.5))
    stim.delay = 50
    stim.dur = 200
    stim.amp = 0.5
    nrn.tstop = 400
